(function() {
  jQuery(function($) {
    return $(window).stellar({
      horizontalScrolling: false,
      hideDistantElements: false
    });
  });

}).call(this);
