jQuery ($) ->

  $(window).stellar
    horizontalScrolling: false
    hideDistantElements: false

  # $('.nav-fiction').hover () ->
  #   $('.nav-dropdown-fiction').show()
  #   $('nav').css 'height', '+=200px'
  # , () ->
  #   $('.nav-dropdown-fiction').hide()
  #   $('nav').css 'height', '-=200px'
  #
  # $('.nav-poetry').hover () ->
  #   $('.nav-dropdown-poetry').show()
  #   $('nav').css 'height', '+=200px'
  # , () ->
  #   $('.nav-dropdown-poetry').hide()
  #   $('nav').css 'height', '-=200px'
  #
  # $('.nav-non-fiction').hover () ->
  #   $('.nav-dropdown-non-fiction').show()
  #   $('nav').css 'height', '+=200px'
  # , () ->
  #   $('.nav-dropdown-non-fiction').hide()
  #   $('nav').css 'height', '-=200px'
